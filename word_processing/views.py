from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse
from django.views.decorators.csrf import csrf_protect
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.status import (
    HTTP_200_OK,
)
from server.get_key_words_lda import get_key_words_lda
from server.get_key_words_bert import get_key_words_bert
from package.models import File
from django.conf import settings
import os
from api.models import Words

@csrf_protect
@api_view(['POST'])
def word_page(request):
	text = request.data.get('text')
	file = request.data.get('file')
	words_num = 1
	topics = 1
	if request.data.get('words').isnumeric():
		words_num = request.data.get('words')

	if request.data.get('topics').isnumeric():
		topics = request.data.get('topics')
	if (file):
		text = ''
		new_file = File(file=file)
		new_file.save()
		path = settings.BASE_DIR + '/media/' + str(new_file.file)
		with open(path) as file_handler:
			for line in file_handler:
				text += line
		new_file.delete()
		os.remove(path)
	keywords = []

	getkey = get_key_words_lda(text, int(topics), int(words_num))
	keywords.append(['LDA', getkey])

	if request.user.is_authenticated:
		entry = Words(text=text, words=" ".join(getkey), user=request.user)
		entry.save()


	return Response({'keywords': keywords}, status=HTTP_200_OK)


# Create your views here.
