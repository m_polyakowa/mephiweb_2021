from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.conf import settings
from rest_framework.status import (
	HTTP_200_OK,
)
from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import FormParser, MultiPartParser
from server.clasters import clasters
from package.models import File
import os
from django.core.files.storage import FileSystemStorage


@api_view(['POST'])
@csrf_protect
@parser_classes([FormParser, MultiPartParser])
def package(request):
	file_list = []
	models_list = []
	files = []
	count = 0
	for f in request.FILES.getlist('files'):
		# filename = f.name
		# new_file = File(file=f)
		# new_file.save()
		# models_list.append(new_file)
		fs = FileSystemStorage()
		filename = fs.save(f.name, f)
		# uploaded_file_url = fs.url(filename)
		file_list.append('build/media/' + filename)
		count += 1
	clusters = int(request.data.get('clusters'))
	if clusters > count:
		clusters = count
	res = clasters(file_list, clusters)
	for i in models_list:
		os.remove('build/media/'+str(i.file))
	return Response({'src': 'media/clasters_grafic.png', 'files': files}, status=HTTP_200_OK)

