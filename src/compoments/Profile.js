import React, {useEffect, useState} from 'react';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import Header from './Header';
import { useAuth } from './AuthProvider';
import {profile} from '../api';

export default function Profile() {
	const {user} = useAuth();
	const [content, setContent] = useState([]);
	useEffect(() => {
		profile().then((res) => {
			setContent(res.content);
		})
	}, []);
	return (
		<>
			<Header />
			<Container>
				<Typography variant="h6">
					Привет,
					{' ' + user}!
				</Typography>
				<Divider />
				{content && content.map((data, idx) => {
					const { text, words } = data;
					return (
					<React.Fragment key={idx}>
						<Typography variant="h6">{words}</Typography>
						<Typography>{text}</Typography>
						<Divider />
					</React.Fragment>
					);
				})}
			</Container>
		</>
	);
}