import React, {useState} from 'react';
import {useForm, Controller} from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Header from './Header';
import {wordProcessing} from '../api';

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    error: {
        color: 'red',
    },
}));

export default function Word() {
    const {control, handleSubmit} = useForm();
    const [error, setError] = useState('');
    const [result, setResult] = useState(null);
    const classes = useStyles();
    const submit = async (data, e) => {
        wordProcessing(data)
        .then((data) => {
            const {keywords} = data;
            setResult(keywords[0][1]);
        })
        .catch((e) => {
            setError('Что-то пошло не так!')
        })
    };
  return (
    <>
        <Header />
        <Container>
            <Typography align="center" variant="h6">Определение ключевых слов документа</Typography>
            {!result && (
            <>
            <form noValidate className={classes.form} onSubmit={handleSubmit(submit)}>
            <Controller
            name="text"
            control={control}
            defaultValue=""
            render={({ field: { onChange, value }, fieldState: { error } }) => (
                <TextField
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    multiline
                    rows="6"
                    name="text"
                    id="text"
                    label="Текст"
                    value={value}
                    onChange={onChange}
                    error={!!error}
                    helperText={error ? error.message : null}
                />
                )}
                rules={{ required: 'Поле обязательно для заполнения' }}
            />

            <Controller
            name="topics"
            control={control}
            defaultValue=""
            render={({ field: { onChange, value }, fieldState: { error } }) => (
                <TextField
                    type="number"
                    margin="normal"
                    variant="outlined"
                    name="topics"
                    id="topics"
                    label="Количество тем"
                    value={value}
                    onChange={onChange}
                    error={!!error}
                    helperText={error ? 'Значение от 1 до 10' : null}
                />
                )}
                rules={{
                required: true,
                min: 1,
                max: 10,
                }}
            />

            <Controller
            name="words"
            control={control}
            defaultValue=""
            render={({ field: { onChange, value }, fieldState: { error } }) => (
                <TextField
                    type="number"
                    margin="normal"
                    variant="outlined"
                    name="words"
                    id="words"
                    label="Количество слов в каждой теме"
                    value={value}
                    onChange={onChange}
                    error={!!error}
                    helperText={error ? 'Значение от 1 до 10' : null}
                />
                )}
                rules={{
                required: true,
                min: 1,
                max: 10,
                }}
            />

                <Button fullWidth type="submit">Отправить</Button>
            </form>
            <div className={classes.error}>{error}</div>
            </>
            )}
            {result && result.map((data, idx) => {
            return (
            <React.Fragment key={idx}>
                <Typography variant="h6">{data}</Typography>
                <Divider />
            </React.Fragment>
            );
            })}
        </Container>
    </>
  );
}