from rest_framework import serializers

from api.models import Words

class WordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Words
        fields = '__all__'
