from django.db import models
from django.contrib.auth.models import User

class Words(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    words = models.TextField(max_length=1000)
    text = models.TextField()

    def __str__(self):
        return self.text

    def get_words(self):
        return self.words