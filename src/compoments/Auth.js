import {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import {useAuth} from './AuthProvider';

const useStyles = makeStyles((theme) => ({
    progress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
    },
}));

const Auth = ({children}) => {
    const auth = useAuth();
    const [state, setState] = useState('loading');
    const classes = useStyles();
	useEffect(() => {
        auth.getData(() => {
            setState('ready');
        });
    }, []);
    return (
        <>
            {state === 'loading' && <CircularProgress className={classes.progress} />}
            {state === 'ready' && children}
        </>
    );
};

export default Auth;
