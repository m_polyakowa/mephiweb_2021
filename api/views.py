from django.http import JsonResponse, HttpResponseNotAllowed,  HttpResponseBadRequest, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required
from .models import Words
from .serializers import UserSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_408_REQUEST_TIMEOUT
)

@api_view(['GET'])
@login_required
def profile(request):
    content = []
    words = Words.objects.all()
    filtered = words.filter(user=request.user)
    if filtered:
        content = [{'text': w.text, 'words': w.words} for w in filtered]
    return Response({'content': content}, status=HTTP_200_OK)

@api_view(['POST'])
def create_user(request):
    user = UserSerializer(data=request.data)
    if user.is_valid():
        user = user.save()
        user.save()
    else:
        return Response(user.errors, status=HTTP_400_BAD_REQUEST)
    login(request._request, user)
    return Response(user.username, status=HTTP_201_CREATED)

@api_view(['POST'])
def login_user(request):
    user = authenticate(request, username=request.data.get('username', ''), password=request.data.get('password', ''))
    if user is not None:
        login(request._request, user)
        return Response({'username': user.username}, status=HTTP_200_OK)
    else:
        return Response(status=HTTP_404_NOT_FOUND)

@api_view(['GET'])
def logout_user(request):
    logout(request)
    return Response(status=HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user(request):
    return Response({'username': request.user.username, 'is_staff': request.user.is_staff}, status=HTTP_200_OK)

@api_view(['GET'])
def users_list (request):
    users = User.objects.all()
    resp = [{u.id: u.username} for u in users]
    return JsonResponse({"resp": resp})
