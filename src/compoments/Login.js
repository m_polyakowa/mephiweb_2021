import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Header from './Header';
import {useAuth} from './AuthProvider';

const useStyles = makeStyles((theme) => ({
    error: {
        color: 'red',
    },
}));

export default function Login() {
    const auth = useAuth();
    const history = useHistory();

    const {register, handleSubmit, errors} = useForm();
    const classes = useStyles();
    const [error, setError] = useState('');

    const login = async (data) => {
        auth.signin(data, '/api/login/',
            () => {
                history.replace('/profile');
            },
            (err) => {
                setError(err.username || err.password || 'Что-то пошло не так');
            }
        );
    };

    return (
        <>
            <Header />
            <Container maxWidth="xs">
                <h1>
                    Войти
                </h1>
                <form onSubmit={handleSubmit(login)} noValidate>
                    <TextField
                        required
                        fullWidth
                        variant="outlined"
                        margin="normal"
                        name="username"
                        label="Юзернейм"
                        id="username"
                        {...register('username', {
                            required: true,
                        })}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        required
                        fullWidth
                        variant="outlined"
                        margin="normal"
                        name="password"
                        label="Пароль"
                        type="password"
                        id="password"
                        {...register('password', {
                            required: true,
                        })}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />

                    <Button fullWidth type="submit">Войти</Button>
                </form>
                <p className={classes.error}>{error}</p>
            </Container>
        </>
    );
}
