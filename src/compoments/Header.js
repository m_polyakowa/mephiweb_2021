import React from 'react';
import {Link, useHistory} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'
import { useAuth } from './AuthProvider';

export default function Header() {
    const auth = useAuth();
    const history = useHistory();
    const logout = async () => {
        auth.signout(() => {
            history.replace('/');
        },
        () => console.error('logout error')
        );
    };
    return (
        <AppBar position="static">
            <Toolbar>
                <Button component={ Link } to="/">О приложении</Button>
                <Button component={ Link } to="/word">Определение ключевых слов документа</Button>
                <Button component={ Link } to="/package">Пакетная обработка документов</Button>
                {auth.user && (
                    <>
                    <Button component={ Link } to="/profile">Профиль</Button>
                    <Button onClick={logout}>Выйти</Button>
                    </>
                )}
                {!auth.user && (
                    <>
                    <Button component={ Link } to="/login">Войти</Button>
                    <Button component={ Link } to="/sign-up">Зарегистрироваться</Button>
                    </>
                )}
            </Toolbar>
        </AppBar>
    );
}
