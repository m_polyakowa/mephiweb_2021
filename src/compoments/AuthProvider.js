import React, { useContext, createContext, useState } from "react";
import {
  Route,
  Redirect,
} from "react-router-dom";
import {login, logout, getUser} from '../api';

// https://reactrouter.com/web/example/auth-workflow

const authContext = createContext();

export default function ProvideAuth({ children }) {
    const auth = useProvideAuth();
    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
}

export function useAuth() {
    return useContext(authContext);
}

function useProvideAuth() {
    const [user, setUser] = useState(null);

    const signin = (credentials, url, succ, err) => {
        login(credentials, url, err)
        .then(json => {
            setUser(json.username);
            succ();
        })
        .catch((e) => {
            err(e);
        })
    };

    const signout = cb => {
        logout()
        .then(() => {
            setUser(null);
            cb();
        });
    };

    const getData = cb => {
        getUser()
        .then(json => {
            setUser(json.username);
            cb();
        })
        .catch(() => {
            setUser(null);
        })
    };

    return {
        user,
        signin,
        signout,
        getData,
    };
}

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export function PrivateRoute({ children, ...rest }) {
    let auth = useAuth();
    return (
        <Route
        {...rest}
        render={({ location }) =>
            auth.user ? (
            children
            ) : (
            <Redirect
                to={{
                pathname: "/login",
                state: { from: location }
                }}
            />
            )
        }
        />
    );
}
