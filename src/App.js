import {
	BrowserRouter as Router,
	Switch,
	Route,
} from 'react-router-dom';
import {ThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import blueGrey from '@material-ui/core/colors/blueGrey';
import './App.css';
import Auth from './compoments/Auth';
import AuthProvider, {PrivateRoute} from './compoments/AuthProvider'
import Main from './compoments/Main';
import Login from './compoments/Login';
import SignUp from './compoments/SignUp';
import Package from './compoments/Package';
import Profile from './compoments/Profile';
import Word from './compoments/Word';

function App() {
	const theme = createMuiTheme({
		palette: {
		  primary: {
			main: blueGrey[600],
		  },
		  secondary: {
			main: indigo[100],
		  },
		},
	  });
	return (
		<ThemeProvider theme={theme}>
			<AuthProvider>
				<Auth>
					<Router>
						<Switch>
							<Route path="/login">
								<Login />
							</Route>
							<Route path="/sign-up">
								<SignUp />
							</Route>
							<Route path="/package">
								<Package />
							</Route>
							<Route path="/word">
								<Word />
							</Route>
							<PrivateRoute path="/profile">
								<Profile />
							</PrivateRoute>
							<Route path="/">
								<Main />
							</Route>
						</Switch>
					</Router>
				</Auth>
			</AuthProvider>
		</ThemeProvider>
	);
}

export default App;
