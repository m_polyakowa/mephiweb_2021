from django.urls import path, include
import django.contrib.auth.urls
from django.contrib import admin
from django.contrib.auth import views as auth_views
from .views import create_user, profile, login_user, logout_user, get_user
import package.urls
import word_processing.urls

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('create/', create_user, name='create_user'),
    path('profile/', profile, name='profile'),
    path('login/', login_user, name='login'),
    path('logout/', logout_user, name='logout'),
    path('get_user/', get_user, name='get_user'),
    path('admin/', admin.site.urls),
    path('word_processing/', include('word_processing.urls')),
    path('package_processing/', include('package.urls')),
]