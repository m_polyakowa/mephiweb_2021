import {getCookie} from './utils';

export const login = (data, url, errCb) => {
    const token = getCookie('csrftoken');
    return fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json',
        'X-CSRFToken': token,
        },
        body: JSON.stringify(data)
    })
    .then(res => {
        if (res.ok) {
            return res.json()
        } else {
            throw new Error('Что-то пошло не так');
        }
    });
}

export const logout = () => {
    const token = getCookie('csrftoken');
    return fetch('/api/logout/', {
        method: 'GET',
        credentials: 'include',
        headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json',
        'X-CSRFToken': token,
        },
    })
    .then(res => res)
}

export const getUser = () => {
    const token = getCookie('csrftoken');
    return fetch('/api/get_user/', {
        method: 'GET',
        credentials: 'include',
        headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json',
        'X-CSRFToken': token,
        },
    })
    .then(res => res.json())
}

export const profile = () => {
    const token = getCookie('csrftoken');
    return fetch('/api/profile/', {
        method: 'GET',
        credentials: 'include',
        headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json',
        'X-CSRFToken': token,
        },
    })
    .then(res => {
        if (res.ok) {
            return res.json()
        } else {
            throw new Error('Что-то пошло не так');
        }
    });
}

export const wordProcessing = (data) => {
    const token = getCookie('csrftoken');
    return fetch('/api/word_processing/', {
        method: 'POST',
        credentials: 'include',
        headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json',
        'X-CSRFToken': token,
        },
        body: JSON.stringify(data)
    })
    .then(res => {
        if (res.ok) {
            return res.json()
        } else {
            throw new Error('Что-то пошло не так');
        }
    });
}

export const packageProcessing = (data) => {
    const token = getCookie('csrftoken');
    const options = {
        method: 'POST',
        credentials: 'include',
        headers: {
        'X-CSRFToken': token,
        },
        body: data
    };
    return fetch('/api/package_processing/', options).then(res => res.json())
}

