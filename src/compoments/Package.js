import React, {useState} from 'react';
import {useForm, Controller} from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Header from './Header';
import {packageProcessing} from '../api';

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    error: {
        color: 'red',
    },
    button: {
        margin: '40px',
    },
    image: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100%',
    },
}));

export default function Word() {
    const {control, handleSubmit} = useForm();
    const [error, setError] = useState('');
    const [result, setResult] = useState(null);
    const [isPicked, setPicked] = useState(false);
    const [files, setFiles] = useState(null);
    const [numFiles, setNumFiles] = useState(0);
    const classes = useStyles();
    const submit = async (data) => {
        const {clusters} = data;
        const formData = new FormData();
        for (let i = 0; i < files.length; i++) {
            formData.append('files', files[i], files[i].name)
        }
        formData.append('clusters', clusters);
        packageProcessing(formData)
        .then((data) => {
            setResult(data);
        })
        .catch((e) => {
            setError('Что-то пошло не так!')
        })
    };
  const uploadHandler = (event) => {
    const { target } = event;
    if (target && target.files) {
        setPicked(true);
        setFiles(target.files);
        setNumFiles(target.files.length);
    }
  };
  return (
      <>
      <Header />
        <Container maxWidth="xs">
                <Typography align="center" variant="h6">Тематическая близость нескольких текстов</Typography>
                {!result && (
                <>
                    <form noValidate className={classes.form} onSubmit={handleSubmit(submit)}>
                        <Typography>
                            Загрузите файл
                        </Typography>
                        <input type="file" name="file" multiple onChange={uploadHandler} />

                        {isPicked && (
                        <>
                            <Typography>Введите количество кластеров(не более количества файлов</Typography>
                            <Controller
                                name="clusters"
                                control={control}
                                defaultValue="1"
                                render={({ field: { onChange, value }, fieldState: { error } }) => (
                                    <TextField
                                        fullWidth
                                        type="number"
                                        margin="normal"
                                        variant="outlined"
                                        name="clusters"
                                        id="clusters"
                                        label="Количество кластеров"
                                        value={value}
                                        onChange={onChange}
                                        error={!!error}
                                        helperText={error ? 'Значение от 1 до количества файлов' : null}
                                    />
                                )}
                                rules={{
                                required: true,
                                min: 1,
                                max: numFiles,
                                }}
                            />
                        </>)}

                        <Button
                        variant="contained"
                        type="submit"
                        className={classes.button}
                        >
                        Отправить
                        </Button>
                    </form>
                    <div className={classes.error}>{error}</div>
                </>
                )}
        </Container>
        {result && <img src="http://127.0.0.1:8000/media/clasters_grafic.png" className={classes.image}/>}
      </>
  );
}
